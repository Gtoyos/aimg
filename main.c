/*
AIMG (image to ascii generator). Modes:
-h help
-p persistent (Called Viewer mode in interface)
-i interactive
use: itag (mode) (FLAGS) images
Use -h to get the help message
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <pthread.h>
#include <signal.h>

#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_RESIZE_IMPLEMENTATION
#include "./lib/stb_image.h"
#include "./lib/stb_image_resize.h"
#include "./lib/gifdec.h"

#define MAXIMG 100 // Max images to process per execution
#define MAXNAME 100 // Max filename length 
#define PROC_DELAY 0 //Relative delay in GIF computing

#define HELPMESSAGE "---AImg Image to ASCII converter. ---\n Use: %s (-mode -flags ...) images...\n Modes: \n\t-i :interactive: Generate the ascii art using a cli-driven menu. \
Easy to configure.\n\t-v (or p): viewer: dispays the generated image on stdout without quitting always trying to fit the console (can be overriden by -n). If the console is resized, the ASCII \
image is generated again.\n\nFlags: \n\t -n nativeres: generates ASCII image in the same dimensions of the original image \n\t -r widthxlength: generates the ASCII \
image in a fixed resolution.\n\t -f: multifile: saves each requested image in a sepparated file\n\t -l1/l2 : choose ASCII palette to generate image. There are currently two available\n\t -e1/e2 : \
Choose formula to calculate pixel luminosite. Currently formulas are ITU BT.709/BT.601\n"

//Pixel->ASCII palletes
#define PALLETE1 "`^\",:;Il!i~+_-?][}{1)(|\\/tfjrxnuvczXYUJCLQ0OZmwqpdbkhao*#MW&8%B@$" //PS = 0;
#define PALLETE2 ".'`^\",:;Il!i><~+_-?][}{1)(|\\/tfjrxnuvczXYUJCLQ0OZmwqpdbkhao*#MW&8%B@$" //PS = 1;
//RGB->Luma Conversion formulas
#define BT709(R,G,B) 0.2126*(R) + 0.7152*(G) + 0.0722*(B) //ITU BT.709 MF=0
#define BT601(R,G,B) 0.299*(R) + 0.587*(G) + 0.114*(B)    //ITU BT.601 MF=1

typedef struct{
    unsigned int mh :1; //help mode
    unsigned int mi :1; //interactive mode
    unsigned int mp :1; //viewer mode
    unsigned int fr :1; //resolution flag
    unsigned int ff :1; //file flag
    unsigned int fn :1; //native resolution flag
    unsigned int ps :1; //palette select (2 palettes)
    unsigned int mf :1; //maping formula (2 formulas)
} Flags;

typedef struct{
    int key;
    int deltascreen;
    int imgchange;
} Varstack;

typedef struct{
    int x;
    int y;
} Cartesian;

typedef struct x{
    char *frame;
    int delay;
    struct x *nxt;
} * Framenode;

int mapimg(char *, Cartesian *); 
int parseparams(int, char **,int[] , Cartesian *);
int interactive_mode(int,char**,int[], Cartesian);
int persistent_mode(int,char**,int[], Cartesian);
int errorchk(int ,char *,char *, Cartesian *);
float bt709(int *, int);
float bt601(int *, int);
void * get_keyinput();
void signalhand(int);

Flags flags = {0,0,0,0,0,0,0,0};
Varstack stack;

int main(int argc, char **argv){
    int images[MAXIMG],state=0;
    Cartesian res;
    unsigned char * pixel;
    unsigned x,y,comp;

    state = parseparams(argc,argv,images,&res);
    state = errorchk(state,argv[0],NULL,NULL);
    if (state)
        return state;
    if (flags.mh){
        fprintf(stderr, HELPMESSAGE,argv[0]);
        return 0;
    }
    else if (flags.mi)
        interactive_mode(argc,argv,images,res);
    else if (flags.mp)
        persistent_mode(argc,argv,images,res);
    else{
        for(int i=0; images[i] != -1; i++){
            state = mapimg(argv[images[i]],&res);
            errorchk(state,argv[0],argv[images[i]],NULL);
            if (state)
                return state;
            if ((strstr(argv[images[i]],".gif") != NULL) && !flags.ff)
                fprintf(stdout,"This is a preview of the GIF image. To view it animated use: %s -v %s (viewer mode).",argv[0],argv[images[i]]);
            if (!flags.ff)
                putchar('\n');
        }
    }
    return 0;
}

int mapimg(char *filename, Cartesian * newres){
    int dispascii(unsigned char *pixels,int width,int height,int comp,int pallete,float (*)(int *, int),FILE * output);
    int width,height,comp,palette;
    float (*formula)(int *, int);
    float light,ratio,nx,ny;
    char letter;
    char *fileout,*dotdot;
    struct winsize w;
    FILE * output;
    unsigned char * pixels,*repixels;
    pixels = stbi_load(filename,&width,&height,&comp,0);

    if (pixels == NULL)
        return 2;
    if (flags.ps)
        palette = 2;
    else
        palette = 1;
    if (flags.mf)
        formula = &bt601;
    else
        formula = &bt709;

    if (flags.ff){
        fileout = (char *)malloc(strlen(filename)+5); // filename.txt
        strcpy(fileout,filename);
        dotdot = strchr(fileout,'.');
        *dotdot = '\0';
        strcat(fileout,".txt");
        output = fopen(fileout,"w");
        if (output == NULL){
            errorchk(4,fileout,NULL,NULL);
            return 4;
        }
    }
    else{
        output = stdout;
    }
    if (flags.fn)
        dispascii(pixels,width,height,comp,palette,formula,output);
    else if (flags.fr){
        repixels = (char *) malloc(newres->x * newres->y * comp);
        if(!stbir_resize_uint8(pixels,width,height,0,repixels,newres->x,newres->y,0,comp))
            return 3;
        
        dispascii(repixels,newres->x,newres->y,comp,palette,formula,output);
        free((void *) repixels);
    }
    else{
        ratio = (float)width/(float)height;
        ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
        if (!w.ws_col && !w.ws_row) //Possible redirection of stdout. Print native
            dispascii(pixels,width,height,comp,palette,formula,output);
        if (width > (w.ws_col/2)){
            nx = (float)w.ws_col/2;
            ny = nx/ratio;
            if(ny > w.ws_row){
                ny = (float)w.ws_row;
                nx = ny*ratio;
            }
            newres->x = (int)nx;
            newres->y = (int)ny;
        }
        else if (height > w.ws_row){
            ny = (float)w.ws_row;
            nx = ny*ratio;
            if (nx > (w.ws_col/2)){
                nx = (float)w.ws_col/2;
               ny = nx/ratio;
            }
            newres->x = (int)nx;
            newres->y = (int)ny;        
        }
        else{
            newres->x = width;
            newres->y = height;
        }
        repixels = (char *) malloc(newres->x * newres->y * comp);
        if (flags.mp){ //Reduce resolution to fit persistent menu.
            newres->x -=2;
            newres->y -=2;
        }
        if(!stbir_resize_uint8(pixels,width,height,0,repixels,newres->x,newres->y,0,comp))
            return 3;
        dispascii(repixels,newres->x,newres->y,comp,palette,formula,output);
        free((void *) repixels);
    }
    free((void *) pixels);
    if (flags.ff)
        fclose(output);
    return 0;
}

int dispascii(unsigned char *pixels,int width,int height,int comp,int pallette,float (*formula)(int *,int), FILE * output){
    int len,k;
    float light;
    char letters[70],letter;
    int *pix;

    if (pallette == 1)
        strcpy(letters,PALLETE1);
    else if (pallette == 2)
        strcpy(letters,PALLETE2);
    len = strlen(letters) -1;
    pix = (int *) malloc(sizeof(int)*comp);

    for(int j=1; j <= height; j++){
        for(int i=1; i <= width; i++){
            for (k=0; k < comp; k++)
                *(pix++) = pixels[k];
            pix = pix-comp;
            light = (*formula)(pix,comp);
            letter = letters[(int)floor(light*((float)len/255.0))];
            putc(letter,output);putc(letter,output);
            pixels = pixels+comp;
        }
        putc('\n',output);
    }
    free((void *) pix);
    return 0;
}
float bt709(int * pix, int comp){
    //RGB & RGBA
    if (comp == 3)
        return BT709(*pix,*(pix+1),*(pix+2));
    else if (comp == 4)
        return ((BT709(*pix,*(pix+1),*(pix+2)))*(*(pix+3)/255.0));
    //Grayscale and Grayscale+alpha
    else if (comp == 2)
        return (*pix)*(*(pix+1)/255.0);
    else if (comp == 1)
        return *pix;
}
float bt601(int * pix, int comp){
    //RGB & RGBA
    if (comp == 3)
        return BT601(*pix,*(pix+1),*(pix+2));
    else if (comp == 4)
        return ((BT601(*pix,*(pix+1),*(pix+2)))*(*(pix+3)/255.0));
    //Grayscale and Grayscale+alpha
    else if (comp == 2)
        return (*pix)*(*(pix+1)/255.0);
    else if (comp == 1)
        return *pix;
}

int parseparams(int argc, char **argv,int images[], Cartesian * res){
    int j,f,n,i=1,k=0;
    char *str = NULL;
    images[k] = -1;
    while (argc-- > 1){
        if (argv[i][0] == '-'){
            for (j=1; argv[i][j]; j++){
                switch (argv[i][j]){
                    case 'h':
                        flags.mh = 1;
                        return 0;
                        break;
                    case 'v': case 'p':
                        flags.mp = 1;
                        break;
                    case 'i':
                        flags.mi =1;
                        break;
                    case 'r':
                        flags.fr =1;
                        i++;
                        argc--;
                        for(n=1;argv[i][n] != 'x'; n++);
                        str = (char *) malloc(n+1);
                        strncpy(str,argv[i],n);
                        str[n+1] = '\0';
                        res->x = atoi(str);
                        for(f=0,n=n+1;argv[i][n];n++,f++)
                            str[f] = argv[i][n];
                        str[f]='\0';
                        res->y = atoi(str);
                        argv[i][j+1] = '\0'; //stop iterating argv[i]
                        break;
                    case 'l':
                        if(!isdigit(argv[i][++j])) return 1;
                        else{
                            if (argv[i][j] == '2')
                                flags.ps = 1;
                            else if (argv[i][j] == '1')
                                flags.ps = 0;
                            else return 1;
                            argv[i][j+1] = '\0';
                        }
                        break;
                    case 'e':
                        if(!isdigit(argv[i][++j])) return 1;
                        else{
                            if (argv[i][j] == '2')
                                flags.mf = 1;
                            else if (argv[i][j] == '1')
                                flags.mf = 0;
                            else return 1;
                            argv[i][j+1] = '\0';
                        }
                        break;
                    case 'f':
                        flags.ff =1;
                        break;
                    case 'n':
                        flags.fn = 1;
                        break;
                    default:
                        return 1;
                        break;
                }
            }
        }
        else
            images[k++] = i;
        i++;
    }
    images[k] = -1;
    return 0;
}

int interactive_mode(int argc,char ** argv,int images[], Cartesian res){
    int get_human_filesize(char *,char *);
    int changeparam(Cartesian Res);
    int changeimages(int,char**,int *);
    void * getnum();
    struct winsize old,new;
    int x,y,comp,state,statecount=0;
    char humansize[MAXNAME];
    pthread_t getnumt;
    pthread_create(&getnumt,NULL,getnum,NULL);
    while(1){
        ioctl(STDOUT_FILENO, TIOCGWINSZ, &new);
        if (old.ws_col != new.ws_col || old.ws_row != new.ws_row){
            old = new;
            stack.deltascreen = 1;
        }
        else
            stack.deltascreen = 0;
        if (stack.deltascreen || stack.imgchange){
            system("clear");
            for(int i=0; i< new.ws_col/2 - 8; i++)
                putchar('-');
            printf("Interactive-Mode");
            for(int i=0; i< new.ws_col/2 - 8; i++)
                putchar('-');
            putchar('\n');
            for(int i=0; i < new.ws_col; i++)
                putchar('-');
            printf("\nLoaded files:\n");
            printf("\t%-20s\t%10s\t%10s\t%7s\t%9s\n\n","name","resolution","components","size","status");
            for(int i=0; images[i] != -1; i++){
                state = stbi_info(argv[images[i]],&x,&y,&comp);
                if (!state){
                    statecount++;
                    printf("\t%-20s\t%dx%d\t%9d\t%15s\t%9s\n",argv[images[i]],0,0,0,"---","ERROR");
                }
                else{
                    get_human_filesize(argv[images[i]],humansize);
                    printf("\t%-20s\t%dx%d\t%9d\t%15s\t%9s\n",argv[images[i]],x,y,comp,humansize,"OK");
                }
            }
            for(int i=0; i < new.ws_col; i++)
                putchar('-');
            printf("\nParameters:\n");
            printf("\tMode:\t\t%s\n",(flags.mp) ? "Viewer" : "Classic");
            printf("\tResolution:\t");
            if (flags.fr){
                printf("Resize to %dx%d\n",res.x,res.y);
            }
            else if (flags.fn)
                printf("Native\n");
            else
                printf("Fit screen\n");
            printf("\tOutput:\t\t");
            if (flags.ff)
                printf("Save in files\n");
            else
                printf("Standard\n");
            printf("\tPalette:\t");
            if (!flags.ps)
                printf(PALLETE1);
            else if (flags.ps)
                printf(PALLETE2);
            printf("\n\tFormula:\t");
            if (!flags.mf)
                printf("BT709\n");
            else if (flags.mf)
                printf("BT601\n");
            for(int i=0; i < new.ws_col; i++)
                putchar('-');
            printf("\nSelect an option:\n\t1_Generate ASCII images.\n\t2_Modify a parameter\n\t3_Modify file list\n\t4_Exit\n");
        }
        switch (stack.key){
            case '1':
                pthread_join(getnumt,NULL);
                if (flags.mp)
                    persistent_mode(argc,argv,images,res);
                else{
                    for(int i=0; images[i] != -1; i++)
                        mapimg(argv[images[i]],&res);
                }
                if (statecount == 1)
                    fprintf(stderr,"Warning: %d image couldn't be loaded.\n",statecount);
                else if (statecount)
                    fprintf(stderr,"Warning: %d images couldn't be loaded.\n",statecount);
                return 0;
                stack.key = '\0';
                stack.imgchange =1;
                break;
            case '2':
                pthread_join(getnumt,NULL);
                changeparam(res);
                pthread_create(&getnumt,NULL,getnum,NULL);
                stack.imgchange =1;
                stack.key = '\0';
                break;
            case '3':
                pthread_join(getnumt,NULL);
                changeimages(argc,argv,images);
                pthread_create(&getnumt,NULL,getnum,NULL);
                stack.imgchange =1;
                stack.key = '\0';
                break;
            case 'q': case '4':
                pthread_join(getnumt,NULL);
                return 0;
                break;
            case '5':
                pthread_join(getnumt,NULL);
                pthread_create(&getnumt,NULL,getnum,NULL);
                stack.imgchange =0;
                stack.key = '\0';
                break;
            default:
                stack.imgchange = 0;
                break;
        }
        usleep(50000);
    }
    return 0;
}

void * getnum(){
    int c;
    fd_set s_rd;
    while(1){
        FD_ZERO(&s_rd);
        FD_SET(fileno(stdin), &s_rd);
        select(FD_SETSIZE,&s_rd,NULL,NULL, NULL);
        c = getchar();
        if (c == '1' || c == '2' || c == '3' || c == '4' || c  == 'q' || c == '5'){
            stack.key = c;
            pthread_exit(NULL);
        }
    }
}

int get_human_filesize(char * filename,char * humansize){
    struct stat fileinfo;
    int bytes;
    float hbytes;
    char fbytes[MAXNAME]; //Or size;
    stat(filename,&fileinfo);
    if(fileinfo.st_size > 1048576){ //1MB
        hbytes = (float)fileinfo.st_size / 1048576.0;
        sprintf(fbytes,"%.1fMB",hbytes);
    }
    else if(fileinfo.st_size > 1024){ //1KB
        hbytes = (float)fileinfo.st_size / 1024.0;
        sprintf(fbytes,"%.1fKB",hbytes);
    }
    else{
        sprintf(fbytes,"%dB",fileinfo.st_size);
    }
    strcpy(humansize,fbytes);
    return 0;
}

int changeparam(Cartesian res){
    char nums[20];
    int i;
    pthread_t getnumt2;
    char sel;
    struct winsize old,new;
    pthread_create(&getnumt2,NULL,getnum,NULL);
    stack.key = '\0';
    while (1){
        ioctl(STDOUT_FILENO, TIOCGWINSZ, &new);
        if (old.ws_col != new.ws_col || old.ws_row != new.ws_row){
            old = new;
            stack.deltascreen = 1;
        }
        else
            stack.deltascreen = 0;
        if (stack.deltascreen || stack.imgchange){
            system("clear");
            for(int i=0; i < new.ws_col; i++)
                putchar('-');
            printf("\nParameters:\n");
            printf("\t1_Mode:\t\t%s\n",(flags.mp) ? "Viewer" : "Classic");
            printf("\t2_Resolution:\t");
            if (flags.fr){
                printf("Resize to %dx%d\n",res.x,res.y);
            }
            else if (flags.fn)
                printf("Native\n");
            else
                printf("Fit screen\n");
            printf("\t3_Output:\t");
            if (flags.ff)
                printf("Save in files\n");
            else
                printf("Standard\n");
            printf("\t4_Palette:\t");
            if (!flags.ps)
                printf(PALLETE1);
            else if (flags.ps)
                printf(PALLETE2);
            printf("\n\t5_Formula:\t");
            if (!flags.mf)
                printf("BT709\n");
            else if (flags.mf)
                printf("BT601\n");
            for(int i=0; i < new.ws_col; i++)
                putchar('-');
            printf("\nWrite parameter's number to modify it or enter q to exit.\n");
        }
        switch (stack.key){
            case '1':
                (flags.mp) ? (flags.mp = 0) : (flags.mp =1);
                stack.key = '\0';
                stack.imgchange =1;
                pthread_join(getnumt2,NULL);
                pthread_create(&getnumt2,NULL,getnum,NULL);
                break;
            case '2':
                getchar(); //catch him!
                printf("Do you want an especific resolution? (y/n)\n");
                pthread_join(getnumt2,NULL);
                while((sel = getchar()) != '\n'){
                    if (sel == 'y' || sel == 'n')
                        break;
                }
                if (sel == 'y'){
                    int i=0;
                    printf("Write an specific resolution in nxn format:\n");
                    getchar(); //catch!
                    while((sel = getchar()) != '\n'){
                        if (isdigit(sel))
                            nums[i++] = sel;
                        else if(sel == 'x'){
                            nums[i] = '\0';
                            res.x = atoi(nums);
                            i = 0;
                        }
                        else{
                            fprintf(stderr,"Bad resolution format.\n");
                            break;
                        }
                    }
                    nums[i] = '\0';
                    res.y = atoi(nums);
                    flags.fr = 1;
                    flags.fn = 0;
                }
                else{
                    (flags.fn) ? (flags.fn = 0) : (flags.fn =1);
                    flags.fr = 0;
                }
                stack.key = '\0';
                stack.imgchange =1;
                pthread_create(&getnumt2,NULL,getnum,NULL);
                break;
            case '3':
                (flags.ff) ? (flags.ff = 0) : (flags.ff =1);
                stack.key = '\0';
                stack.imgchange =1;
                pthread_join(getnumt2,NULL);
                pthread_create(&getnumt2,NULL,getnum,NULL);
                break;
            case '4':
                (flags.ps) ? (flags.ps = 0) : (flags.ps =1);
                stack.key = '\0';
                stack.imgchange =1;
                pthread_join(getnumt2,NULL);
                pthread_create(&getnumt2,NULL,getnum,NULL);
                break;
            case '5':
                (flags.mf) ? (flags.mf = 0) : (flags.mf =1);
                stack.key = '\0';
                stack.imgchange =1;
                pthread_join(getnumt2,NULL);
                pthread_create(&getnumt2,NULL,getnum,NULL);
                break;
            case 'q':
                return 0;
                break;
            default:
                stack.imgchange = 0;
                break;
        }
        usleep(50000);
    }

}

int changeimages(int argc,char ** argv,int images[]){
    int newargvmalloc(int *, char **);
    struct winsize old,new;
    char * newimg;
    int x,y,comp,c,p,j,jpos,delem=0,tmpfilenum;
    char humansize[MAXNAME];
    int state,statecount=0;
    stack.key = '\0';
    pthread_t getnumt;
    pthread_create(&getnumt,NULL,getnum,NULL);
    while(1){
        ioctl(STDOUT_FILENO, TIOCGWINSZ, &new);
        if (old.ws_col != new.ws_col || old.ws_row != new.ws_row){
            old = new;
            stack.deltascreen = 1;
        }
        else
            stack.deltascreen = 0;
        if (stack.deltascreen || stack.imgchange){
            system("clear");
            for(int i=0; i< new.ws_col/2 - 8; i++)
                putchar('-');
            printf("Interactive-Mode");
            for(int i=0; i< new.ws_col/2 - 8; i++)
                putchar('-');
            putchar('\n');
            for(int i=0; i < new.ws_col; i++)
                putchar('-');
            printf("\nLoaded files:\n");
            printf("\t%-20s\t%-20s\t%10s\t%10s\t%7s\t%9s\n\n","file number","name","resolution","components","size","status");
            for(int i=0; images[i] != -1; i++){
                state = stbi_info(argv[images[i]],&x,&y,&comp);
                if (!state){
                    statecount++;
                    printf("\t%-20d\t%-20s\t%dx%d\t%9d\t%15s\t%9s\n",images[i],argv[images[i]],0,0,0,"---","ERROR");
                }
                else{
                    get_human_filesize(argv[images[i]],humansize);
                    printf("\t%-20d\t%-20s\t%dx%d\t%9d\t%15s\t%9s\n",images[i],argv[images[i]],x,y,comp,humansize,"OK");
                }
            }
            for(int i=0; i< new.ws_col; i++)
                putchar('-');
            putchar('\n');
            printf("Select an option:\n\t1_Load new images\n\t2_Remove images\n\t3_exit\n");
        }
        switch(stack.key){
            case '1':
                pthread_join(getnumt,NULL);
                getchar();
                printf("Write file name:\n");
                scanf("%s",humansize);
                newimg = (char *) malloc(strlen(humansize)+1);
                newargvmalloc(&argc,argv);
                strcpy(argv[argc],humansize);
                for(j = 0; images[j] != -1; j++);
                images[j] = argc;
                images[j+1] = -1;
                pthread_create(&getnumt,NULL,getnum,NULL);
                stack.imgchange =1;
                stack.key = '\0';
                break;
            case '2':
                pthread_join(getnumt,NULL);
                getchar();
                printf("Select file number:\n");
                c =0;
                while ((p = getchar()) != '\n')
                    c = c*10 + p - '0';
                for(j=0; images[j] != -1; j++)
                    if (images[j] == c){
                        delem = 1;
                        jpos = j;
                    }
                if (delem){
                    tmpfilenum = images[j-1];
                    images[j-1] = -1;
                    if (c != tmpfilenum)
                        images[jpos] = tmpfilenum;
                }
                pthread_create(&getnumt,NULL,getnum,NULL);
                stack.imgchange =1;
                stack.key = '\0';
                break;
            case '3': case 'q':
                pthread_join(getnumt,NULL);
                return 0;
                stack.imgchange =0;
                stack.key = '\0';
                break;
            case '4':
                pthread_join(getnumt,NULL);
                pthread_create(&getnumt,NULL,getnum,NULL);
                stack.imgchange =0;
                stack.key = '\0';
                break;
            case '5':
                pthread_join(getnumt,NULL);
                pthread_create(&getnumt,NULL,getnum,NULL);
                stack.imgchange =0;
                stack.key = '\0';
                break;
            default:
                stack.imgchange = 0;
                break;
        }
        usleep(50000);
    }
}

int newargvmalloc(int * argc,char ** argv){ //Adds 1 entry to argv
    int i;
    char ** tnewargv;
    tnewargv = (char **) malloc((*argc+1)*sizeof(char *));
    for(i=0; i <= *argc-1; i++){
        tnewargv[i] = argv[i];
    }
    tnewargv[i] = NULL;
    argv = (char **) malloc((*argc + 1) * sizeof(char *));
    for(i=0; i <= *argc-1; i++)
        argv[i] = tnewargv[i];
    *argc += 1;
    return 0;
}

int persistent_mode(int argc,char ** argv,int images[], Cartesian res){
    int playgif(int argc, char ** argv, int images[], int * i, Cartesian *);
    signal(SIGINT,signalhand);
    int i,state,len=0;
    struct winsize old,new;
    pthread_t keyinput_thread;

    for (i=0; images[i] != -1; i++)
        len += 1;
    stack.deltascreen = 1;
    stack.imgchange = 0;
    i =0;
    pthread_create(&keyinput_thread,NULL,get_keyinput,NULL);
    while (1){ //Press q to quit

        ioctl(STDOUT_FILENO, TIOCGWINSZ, &new);
        if (old.ws_col != new.ws_col || old.ws_row != new.ws_row){
            old = new;
            stack.deltascreen = 1;
        }
        else
            stack.deltascreen = 0;
        
        if (stack.deltascreen || stack.imgchange){
            system("clear");
            if (strstr(argv[images[i]],".gif") == NULL){
                state = mapimg(argv[images[i]],&res);
                if (state){
                    state = errorchk(state,NULL,argv[images[i]],&res);
                    return 1;
                }
                printf("----Viewer mode----%s----Image (%d/%d)----Use A/D to move----Press q to quit----",argv[images[i]],(i+1),len);
                for(int j=0; j <= res.x*2 - 77 - (int)strlen(argv[images[i]]); j++)
                    putchar('-');
                putchar('\n');
            }
            else{
                pthread_cancel(keyinput_thread);
                state = playgif(argc,argv,images,&i,&res);
                if (state){
                    errorchk(state,NULL,argv[images[i]],&res);
                    return state;
                }
                if (stack.key == 'q')
                    return 0;
                else{
                    stack.imgchange = 1;
                    stack.key = '\0';
                    pthread_create(&keyinput_thread,NULL,get_keyinput,NULL);
                    continue;
                }
            }
            stack.imgchange = 0;
        }
        switch (tolower(stack.key)){
            case 'd':
                if (i < len-1){
                    i++;
                }
                stack.key = '\0';
                stack.imgchange =1;
                break;
            case 'a':
                if (i > 0){
                    i--;
                }
                stack.key = '\0';
                stack.imgchange =1;
                break;
            case 'q':
                pthread_join(keyinput_thread,NULL);
                return 0;
                break;
            default:
                stack.imgchange = 0;
                break;
        }
        usleep(50000); // 0.05 sec
    }
    return 0;
}

int playgif(int argc, char ** argv, int images[], int * index, Cartesian  * newres){ //to achieve an acceptable performance maybe we can LOAD and RESIZE the GIF first, then render it.
    Framenode loadgif(char *, Cartesian, int *);
    int freegif(Framenode, int);
    Framenode next;
    pthread_t keyinput_thread;
    Cartesian res;
    gd_GIF *gif;
    struct winsize old,new;
    float (*formula)(int *, int);
    float ratio,nx,ny;
    char *repixels,*buffer;
    int len,palette,frames;

    gif = gd_open_gif(argv[images[(*index)]]);
    if (!gif) return 2;
    buffer = malloc(gif->width * gif->height * 3);
    for(len = 0; images[len] != -1; len++);
    (flags.ps) ? (palette = 2) : (palette = 1);
    (flags.mf) ? (formula = &bt601) : (formula = &bt709);

    if (flags.fn){
        res.x = gif->width;
        res.y = gif->height;
        next = loadgif(argv[images[*index]],res,&frames);
        stack.key = '\0';
        pthread_create(&keyinput_thread,NULL,get_keyinput,NULL);
        while(1){
            dispascii(next->frame,res.x,res.y,3,palette,formula,stdout);
            printf("----Viewer mode----%s----Image (%d/%d)----Use A/D to move----Press q to quit----",argv[images[*index]],(*index+1),len);
            for(int j=0; (j <= res.x*2 - 77 - (int)strlen(argv[images[*index]])); j++)
                putchar('-');
            putchar('\n');
            switch (tolower(stack.key)){
                case 'a':
                    pthread_cancel(keyinput_thread);
                    freegif(next,frames);
                    (*index)--;
                    return 0;
                    break;
                case 'd':
                    pthread_cancel(keyinput_thread);
                    (*index)++;
                    freegif(next,frames);
                    return 0;
                    break;
                case 'q':
                    pthread_cancel(keyinput_thread);
                    freegif(next,frames);
                    return 0;
                    break;
                default:
                    break;
            }
            usleep((next->delay) * 10000);
            system("clear");
            next = next->nxt;
        }

    }
    else if (flags.fr){
        res.x = newres->x;
        res.y = newres->y;
        next = loadgif(argv[images[*index]],res,&frames);
        pthread_create(&keyinput_thread,NULL,get_keyinput,NULL);
        while(1){
            dispascii(next->frame,res.x,res.y,3,palette,formula,stdout);
            printf("----Viewer mode----%s----Image (%d/%d)----Use A/D to move----Press q to quit----",argv[images[*index]],(*index+1),len);
            for(int j=0; (j <= res.x*2 - 77 - (int)strlen(argv[images[*index]])); j++) 
                putchar('-');
            putchar('\n');
            switch (tolower(stack.key)){
                case 'a':
                    pthread_cancel(keyinput_thread);
                    freegif(next,frames);
                    (*index)--;
                    return 0;
                    break;
                case 'd':
                    pthread_cancel(keyinput_thread);
                    (*index)++;
                    freegif(next,frames);
                    return 0;
                    break;
                case 'q':
                    pthread_cancel(keyinput_thread);
                    freegif(next,frames);
                    return 5;
                    break;
                default:
                    break;
            }
            usleep((next->delay) * 10000);
            system("clear");
            next = next->nxt;
        }
    }
    else{
        old.ws_col = 0;
        pthread_create(&keyinput_thread,NULL,get_keyinput,NULL);
        while(1){
            ioctl(STDOUT_FILENO, TIOCGWINSZ, &new);
            if (old.ws_col != new.ws_col || old.ws_row != new.ws_row){
                old = new;

                ratio = (float)gif->width/(float)gif->height;

                if (!new.ws_col && !new.ws_row){ //Possible redirection of stdout. Print native
                    fprintf(stderr,"Cant Display animated gif on stdout. Are you in a file?");
                    return 5;
                }
                if (gif->width > (new.ws_col/2)){
                    nx = (float)new.ws_col/2;
                    ny = nx/ratio;
                    if(ny > new.ws_row){
                        ny = (float)new.ws_row;
                        nx = ny*ratio;
                    }
                    res.x = (int)nx;
                    res.y = (int)ny;
                }
                else if (gif->height > new.ws_row){
                    ny = (float)new.ws_row;
                    nx = ny*ratio;
                    if (nx > (new.ws_col/2)){
                        nx = (float)new.ws_col/2;
                    ny = nx/ratio;
                    }
                    res.x = (int)nx;
                    res.y = (int)ny;        
                }
                else {
                    res.x = gif->width;
                    res.y = gif->height;
                }
                stack.deltascreen = 1;
            }
            else
                stack.deltascreen = 0;
            if (stack.deltascreen){
                next = loadgif(argv[images[*index]],res,&frames);
            }
            dispascii(next->frame,res.x,res.y,3,palette,formula,stdout);
            printf("----Viewer mode----%s----Image (%d/%d)----Use A/D to move----Press q to quit----",argv[images[*index]],(*index+1),len);
            for(int j=0; (j <= res.x*2 - 77 - (int)strlen(argv[images[*index]])); j++) 
                putchar('-');
            putchar('\n');
            switch (tolower(stack.key)){
                case 'a':
                    if (*index > 0){
                        (*index)--;
                        pthread_cancel(keyinput_thread);
                        freegif(next,frames);
                        return 0;
                    }
                    break;
                case 'd':
                    if (*index < len-1){
                        pthread_cancel(keyinput_thread);
                        (*index)++;
                        freegif(next,frames);
                        return 0;
                    }
                    break;
                case 'q':
                    pthread_cancel(keyinput_thread);
                    freegif(next,frames);
                    return 0;
                    break;
                default:
                    break;
            }
            usleep((next->delay) * 10000 - PROC_DELAY);
            system("clear");
            next = next->nxt;
        }
    }
}

Framenode loadgif(char * filename,Cartesian res, int * totalframes){
    Framenode cur,next,start;
    gd_GIF *gif = gd_open_gif(filename);
    int frames=0;
    char *repixels,*buffer = malloc(gif->width * gif->height * 3);
    printf("Loading gif into RAM. This could take some seconds...\n");
    //Load initial Frame
    cur = (Framenode) malloc(sizeof(Framenode));
    start = cur;
    gd_get_frame(gif);
    repixels = (char *) malloc(res.x*res.y*3);
    stbir_resize_uint8(buffer,gif->width,gif->height,0,repixels,res.x,res.y,0,3); 
    cur->frame = repixels;
    cur->delay = gif->gce.delay;
    //Iterate trough gif frames
    for (unsigned looped = 1;; looped++) {
        while (gd_get_frame(gif)) {
            frames++;
            gd_render_frame(gif, buffer);
            next = (Framenode) malloc(sizeof(Framenode));
            repixels = (char *) malloc(res.x*res.y*3);
            stbir_resize_uint8(buffer,gif->width,gif->height,0,repixels,res.x,res.y,0,3); 
            next->frame = repixels;
            next->delay = gif->gce.delay;
            cur->nxt = next;
            cur = next;
        }
        //Link end to start (Cyclic linked list)
        cur->nxt = start;
        break;
        if (looped == gif->loop_count)
            break;
        gd_rewind(gif);
    }
    free(buffer);
    gd_close_gif(gif);
    *totalframes = frames;
    return start;
}

int freegif(Framenode data, int frames){
    Framenode tmp;
    //iterate through cyclic list to delete
    for(int i=0; i < frames; i++){
        tmp = data;
        data = data->nxt;
        free((void *) tmp->frame);
        free((void *) tmp);
    }
    return 0;
}

void * get_keyinput(){
    int c;
    fd_set s_rd;
    while(1){
        FD_ZERO(&s_rd);
        FD_SET(fileno(stdin), &s_rd);
        select(FD_SETSIZE,&s_rd,NULL,NULL, NULL);
        c = getchar();
        c = tolower(c);
        if (c == 'q' || c == 'a' || c == 'd')
            stack.key = c;
        if (stack.key == 'q' || stack.key == 'Q')
            pthread_exit(NULL);
    }
}      

int errorchk(int state,char * txt1,char * txt2,Cartesian * res){
    if (state == 1){
        fprintf(stderr,"ERROR: Bad arguments. See %s -h\n",txt1);
        return 1;
    }
    else if (state == 2){
        fprintf(stderr,"ERROR: Can't open file %s\n",txt2);
        return 2;
    }
    else if (state == 3){
        fprintf(stderr,"ERROR: Image can't be resized to %dx%d\n",res->x,res->y);
        return 3;
    }
    else if (state == 4){
        fprintf(stderr,"ERROR: Can't write on file %s.\n",txt1);
        return 4;
    }
    return 0;
}

void signalhand(int sig_num){
    signal(SIGINT, signalhand);
    exit(0);
}

