CC = gcc
CFLAGS = -lm -pthread
LDIR = ./lib
TARGET = aimg

aimg: main.c
	$(CC) $(CFLAGS) $< $(LDIR)/gifdec.c -o $@

.PHONY: install
install:
	mv aimg /usr/bin/
