# Aimg

Displays an image in ASCII terminal format.

## Installation
Use:
``make``
and
``make install``
to compile & install aimg in /usr/bin

## Built With

* [stb_image & stb_image_resize](https://github.com/nothings/stb) - Img loader & resizing 
* [gifdec](https://github.com/lecram/gifdec) - GIF frame loader

## Usage

Syntax: ``aimg (flags) files...``

By default all images are resized to fit the size of the terminal. 

Flags list:

* ``-v`` Viewer Mode.
* ``-i`` Interactive Mode. (Opens a menu that lets you set options and files)
* ``-n`` Native flag. (Display images in their native resolution)
* ``-r nxn`` Display images in nxn resolution.
* ``-f`` Save output in sepparate .txt files per image.

Use ``aimg -h`` for more info.

### Compatibility

Aimg should run on any UNIX environment.
The software has been developed on Debian GNU/Linux

### Contributing

Feel free to make pull requests. All ideas will be discussed and reviewed.

### License

This project is licensed under the GPLv3.0 License - see the [LICENSE.md](LICENSE.md) file for details.